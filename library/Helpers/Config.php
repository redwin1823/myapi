<?php

namespace Helpers;

/* @package library */
class Config {

    /**
     * @var array
     */
    static protected $_config;

    /**
     * @var array valid server environment names
     */
    static protected $_validEnvironments = array(
        'DEVELOPMENT',
        'PRODUCTION'
    );

    /**
     * This class cannot be instantiated
     */
    protected function __construct() {}

    /**
     * Get a config setting
     * @return array
     * @throws Exception
     */
    static public function getConfig() {
        if (is_array(self::$_config)) {
            return self::$_config;
        }

        self::$_config = json_decode(
            file_get_contents(self::getConfigFileName()),
            true
        );
        return self::$_config;
    }

    /**
     * Get config setting by key path
     *
     * Key path is a "." separated string of key/value pairs. For example:
     *
     * ```
     * path.to.config.value
     * ```
     *
     * You can also use numeric array indexes like so:
     *
     * ```
     * path.to.config.0.value
     * ```
     *
     * @param string $key
     * @return int|string|float
     */
    static public function get($key)
    {
        $config = self::getConfig();
        return self::find($key, $config);
    }

    static public function find($key, array $array)
    {
        $keyPath = explode('.', $key);
        return self::_findByPath($keyPath, $array);
    }

    static protected function _findByPath(array $keyPath, array $array)
    {
        if (count($keyPath)) {

            $key = array_shift($keyPath);

            if (is_numeric($key)) {
                $key = (int) $key;
            }

            if (isset($array[$key])) {

                if (is_array($array[$key]) && count($keyPath) > 0) {
                    return self::_findByPath($keyPath, $array[$key]);
                }

                return $array[$key];
            }
        }
        return null;
    }

    /**
     * Get the configuration file name
     * @return string
     * @throws Exception
     */
    static public function getConfigFileName()
    {
        $env = self::getEnvironment();
        $file = self::getApplicationPath() .'/config/' . $env . '.json';

        if (is_file($file) && is_readable($file)) {
            return realpath($file);
        }

        throw new Exception("Cannot find and/or read from configuration file: {$file}");
    }

    /**
     * Get the server environment
     * @return string
     * @throws Exception
     */
    static public function getEnvironment()
    {       
        // FIXME: We don't want to use super globals--ever
        if (isset($_SERVER['ENVIRONMENT'])) {
            $env = $_SERVER['ENVIRONMENT'];
        } else if (defined("APPLICATION_ENV")) {
            $env = APPLICATION_ENV;
        }
        else {
            throw new Exception("Unable to determine server environment. Server is not configured properly.");
        }

        // Did we get an expected environment identifier?
        if (in_array(strtoupper($env), self::$_validEnvironments)) {
            return strtoupper($env);
        }

        throw new Exception("Invalid server environment: '{$env}'.");
    }

    /**
     * Get the application path
     * @return string
     */
    static public function getApplicationPath()
    {
        return realpath(__DIR__ . '/../../');
    }
}