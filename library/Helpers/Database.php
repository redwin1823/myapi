<?php
/**
* API Tools Database Library
*
* @copyright 2019 Japeth Lahaylahay
*/

namespace Helpers;

class Database {
    /**
     * @var PDO
     */
    private $conn;

    public function __construct( $con = 'database.connection', $user = 'database.username', $pass = 'database.password' ) {

        $connString = \Helpers\Config::get($con);
        $username   = \Helpers\Config::get($user);
        $password   = \Helpers\Config::get($pass);

        $this->conn = new \PDO(     $connString,
                                    $username, 
                                    $password,
                                    array(
                                        \PDO::ATTR_ERRMODE               => \PDO::ERRMODE_EXCEPTION,  
                                        \PDO::ATTR_DEFAULT_FETCH_MODE    => \PDO::FETCH_ASSOC 
                                    ) 
                                );
    }

    /**
     * Retrieve all users
     */
    public function getUsers() {
        $stmt = $this->conn->prepare('SELECT * FROM users');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * Insert New User
     */
    public function insertUser($lastname, $firstname, $email) {
        $stmt = $this->conn->prepare('INSERT INTO users(`lastname`,`firstname`,`email`) VALUES(:lastname, :firstname, :email)');
        $stmt->bindParam(':lastname', $lastname, \PDO::PARAM_STR);
        $stmt->bindParam(':firstname', $firstname, \PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, \PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * Update User
     */
    public function updateUser($user_id, $lastname, $firstname, $email) {
        $stmt = $this->conn->prepare('UPDATE users SET lastname=:lastname, firstname=:firstname, email=:email WHERE user_id=:user_id');
        $stmt->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
        $stmt->bindParam(':lastname', $lastname, \PDO::PARAM_STR);
        $stmt->bindParam(':firstname', $firstname, \PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, \PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * Delete User
     */
    public function deleteUser($user_id) {
        $stmt = $this->conn->prepare('DELETE FROM users WHERE user_id=:user_id');
        $stmt->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->rowCount();
    }
}