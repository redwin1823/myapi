<?php
/**
* API Tools Database Library
*
* @copyright 2019 Japeth Lahaylahay
*/

namespace Helpers;

class Json {

	public function __construct() {}

	/**
	 * Generates JSON encoded response based on parameters passed
	 *
	 * @return string $response
	 */
	public function defaultResponse($status, $options = '') {
		$default = array(
				'status'  => $status
			);

		$response = array_merge($default, $options);

		return json_encode($response);
	}
}