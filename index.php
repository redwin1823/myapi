<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

header( 'Access-Control-Allow-Headers: Accept, Content-Type, Authorization' );
header( 'Access-Control-Allow-Methods: POST, PUT, DELETE, GET, OPTIONS' );

require_once 'vendor/autoload.php';
require_once 'environment/production.php';

$app = new \Slim\App;
$json = new \Helpers\Json;

$app->get('/api/users/students', function() use ($json) {
    $db = new \Helpers\Database;
    $users = $db->getUsers();

    $response = $json->defaultResponse(
        'success',
        array(
            'users' => $users
        )
    );
    return $response;
});

$app->post('/api/users/add', function(Request $request, Response $response) use($json) {
    $db = new \Helpers\Database;
    $lastname = $request->getParam('lastname');
    $firstname = $request->getParam('firstname');
    $email = $request->getParam('email');

    $users = $db->insertUser($lastname, $firstname, $email);

    $response = $json->defaultResponse(
        'success',
        array(
            'row_count' => $users
        )
    );
    return $response;
});

$app->put('/api/users/update/{user_id}', function(Request $request, Response $response) use($json) {
    $db = new \Helpers\Database;
    $user_id = $request->getAttribute('user_id');
    $lastname = $request->getParam('lastname');
    $firstname = $request->getParam('firstname');
    $email = $request->getParam('email');

    $users = $db->updateUser($user_id, $lastname, $firstname, $email);

    $response = $json->defaultResponse(
        'success',
        array(
            'row_count' => $users
        )
    );
    return $response;
});

$app->delete('/api/users/delete/{user_id}', function(Request $request, Response $response) use($json) {
    $db = new \Helpers\Database;
    $user_id = $request->getAttribute('user_id');
    
    $users = $db->deleteUser($user_id);

    $response = $json->defaultResponse(
        'success',
        array(
            'row_count' => $users
        )
    );
    return $response;
});

$app->run();
?>